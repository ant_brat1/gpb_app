from werkzeug.security import check_password_hash, generate_password_hash
#   from gpb_bank.models import Bank_user, Bank_acc
from flask import Flask, request, render_template, flash, url_for, redirect
from flask_login import login_user, login_required, logout_user, LoginManager, UserMixin
from flask_sqlalchemy import SQLAlchemy
import socket


app = Flask(__name__, template_folder="templates")
app.secret_key = 'krovostok'
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:Lfirbyp1@db:5432/GPB_test'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
manager = LoginManager(app)


class Bank_user(db.Model, UserMixin):
    user_id = db.Column(db.Integer, primary_key=True)
    user_fio = db.Column(db.String(32), nullable=False)
    user_email = db.Column(db.String(32), nullable=False)
    user_ac_name = db.Column(db.String(32), nullable=False)
    user_pass = db.Column(db.String, nullable=False)

    def __init__(self, user_fio, user_email, user_ac_name, user_pass):
        self.user_fio = user_fio.strip()
        self.user_email = user_email.strip()
        self.user_ac_name = user_ac_name.strip()
        self.user_pass = user_pass.strip()

    def get_id(self):
        return self.user_id


class Bank_acc(db.Model):
    acc_id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('bank_user.user_id'), nullable=False)
    user_login = db.relationship('Bank_user', backref=db.backref('tags', lazy=True))
    acc_num = db.Column(db.Integer, nullable=False)
    acc_cur = db.Column(db.String(5), nullable=False)
    acc_sum = db.Column(db.Integer, nullable=False)

    def __init__(self, user_id, user_login, acc_num, acc_curr, acc_sum):
        self.user_id = user_id.strip()
        self.user_login = user_login.strip()
        self.acc_num = acc_num.strip()
        self.acc_curr = acc_curr.strip()
        self.acc_sum = acc_sum.strip()

    def get_id(self):
        return self.acc_id


@app.route("/", methods=['GET', 'POST'])
def index():
    if request.method == 'GET':
        return render_template('start.html')


@manager.user_loader
def load_user(user_id):
    return Bank_user.query.get(user_id)


@app.route("/registration", methods=['GET', 'POST'])
def registration_site():
    if request.method == 'GET':
        img_path = url_for("static", filename="images/image.png")
        return render_template('registration.html', messages=Bank_user.query.all(), img_path=img_path)


@app.route("/login", methods=['GET', 'POST'])
def login_page():
    login = request.form.get('username')
    password = request.form.get('pwd')

    if request.method == 'POST':
        if login and password:
            user = Bank_user.query.filter_by(user_ac_name=login).first()
            if user and check_password_hash(generate_password_hash(user.user_pass), password):
                login_user(user)
                next_page = request.args.get('next')
                print('1', request.args)
                return redirect(next_page)
            else:
                flash('Login or Password is incorrect')
        else:
            flash('Please fill login and password fields')
    else:
        flash(socket.gethostname())

    img_path = url_for("static", filename="images/image.png")
    return render_template('index.html', img_path=img_path)


@app.route("/api/registration", methods=['GET', 'POST'])
def registration():
    if request.method == 'POST':
        fio = request.form['fio']
        email = request.form['email']
        username = request.form['username']
        pwd = request.form['pwd']

        db.session.add(Bank_user(fio, email, username, pwd))
        db.session.commit()
        return redirect(url_for("index"))


@app.route("/account", methods=['GET', 'POST'])
@login_required
def account():
    if request.method == 'GET':
        img_path = url_for("static", filename="images/image.png")
        return render_template('account.html', img_path=img_path, messages=Bank_acc.query.all())


@app.after_request
def redirect_to_signin(response):
    if response.status_code == 401:
        print('2/1', request.args)
        return redirect(url_for("login_page") + '?next=' + request.url)
    print('2/2', request.args)
    return response


@app.route("/logout", methods=['GET', 'POST'])
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=5000)
